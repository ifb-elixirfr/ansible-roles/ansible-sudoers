Sudoers
===

A role to 
- install sudoers.d files on Ubuntu and CentOS environments
- direct sudo logging to a dedicated file (as well as to /var/log/auth.log file in Ubuntu, and /var/log/secure in CentOS.)


Dependencies
------------

None

Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

This playbook will direct sudo logging to a dedicated sudo.log file and create two files under /etc/sudoers.d/ to configure two sudoers groups

- admins
- webadmins


```yaml
- hosts: all
  roles:
    - ifb.sudoers
  vars:
    sudo_log_dedicated_file: 'true'
    sudo_log_file_name: 'sudo.log'
    sudoers_d_files_admins:
      admins:
        privileges:
          - name: "%admins"
            priv: "ALL=(ALL) NOPASSWD:ALL"
        defaults:
          - name: "%admins"
            default: "!requiretty"
    sudoers_d_files_webadmins:
      webadmins:
        privileges:
          - name: "%webadmins"
            priv: "ALL=NOPASSWD: /sbin/service httpd restart,/etc/init.d/httpd restart,/usr/sbin/apache2ctl restart"


```

[License](LICENSE)
